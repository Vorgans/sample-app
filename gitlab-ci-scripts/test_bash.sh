#!/usr/bin/env bats

@test "Check Home page" {
  curl -sfq my-app:8000/ | fgrep Hello
}

@test "Check version URL" {
  curl -sfq my-app:8000/version
}

@test "Check version URL Content" {
  curl -sfq my-app:8000/version | fgrep $CI_COMMIT_SHORT_SHA